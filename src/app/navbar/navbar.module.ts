import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { NavbarComponent } from './navbar.component'
import {CartService} from '../services/cart.service'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    NgbModule
  ],
  declarations: [NavbarComponent],
  exports: [ NavbarComponent ],
  providers: [
    CartService
  ]
})
export class NavbarModule { }
