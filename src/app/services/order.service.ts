import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http'
import {Order, CartItem} from '../interfaces'
import {environment} from '../../environments/environment'
import {Observable} from 'rxjs'

@Injectable()
export class OrderService {
  url: string = environment.api_url
  
  constructor(private http: Http) { }

  createOrder(order: Order) {
    let body = JSON.stringify(order)
    
    return this.http.post(`${this.url}/orders`, body)
      .map(res => res.json())
      .catch(err => Observable.throw(err.json().error))
  }
  
  orderLines(cart: CartItem[]) {
    let cartString = JSON.stringify(cart)
    let params = encodeURIComponent(cartString)
    
    return this.http.get(`${this.url}/order-lines/${params}`)
      .map(res => res.json().order)
  }
}
