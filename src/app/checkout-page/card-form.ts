import {FormDetails} from '../form-control/form-control.component'

export let CardForm: FormDetails[] = [
  {
    key:'number',
    label:'Kortnummer',
    placeholder:'1234 1234 1234 1234',
    class: 'col-8 col-lg-6 pull-lg-3'
  }, {
    key:'cvc',
    label:'CVC',
    placeholder:'123',
    class: 'col-4 col-lg-3 pull-lg-3'
  }, {
    key:'expMonth',
    label:'Utløps måned',
    placeholder:'MM',
    class: 'col-6 col-lg-3'
  }, {
    key:'expYear',
    label:'Utløps år',
    placeholder:'ÅÅ',
    class: 'col-6 col-lg-3'
  }
]