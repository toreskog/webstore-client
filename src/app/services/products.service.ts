import { Injectable } from '@angular/core'
import {Http} from '@angular/http'
import {Observable} from 'rxjs'
import 'rxjs/add/operator/map'

import {Product} from '../interfaces'
import {environment} from '../../environments/environment'

@Injectable()
export class ProductsService {
  _url: string = environment.api_url
  
  constructor(private _http: Http) {}
  
  
  getAllProducts() : Observable<Product[]>{
    return this._http.get(`${this._url}/products`)
      .map(res => res.json().products)
  }
  
  getProduct(id: string) : Observable<Product>{
    return this._http.get(`${this._url}/products/${id}`)
      .map(res => res.json().product)
  }
  
  getProducts(ids: string[]) : Observable<Product[]>{
    if(ids.length === 0) {
      return Observable.from([[]])
    }
    
    let sids = ids.join()
    
    return this._http.get(`${this._url}/products/${sids}`)
      .map(res => res.json().products)
  }
}
