import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Route, RouterModule} from '@angular/router'

import {ProductsPageComponent} from './products-page.component'
import {ProductCardModule} from '../product-card'
import {ProductsService} from '../services/products.service'

const ROUTES: Route[] = [
  { path: '', component: ProductsPageComponent}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    ProductCardModule
  ],
  declarations: [
    ProductsPageComponent
  ],
  providers: [
    ProductsService
  ]
})
export class ProductsPageModule { }