import {Component, OnInit, NgZone} from '@angular/core'
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import {Router} from '@angular/router'
import {Order, Customer, Address, OrderLine} from '../interfaces'
import {CartService} from '../services/cart.service'
import {OrderService} from '../services/order.service'
import {EmailValidator} from '../validators/email-validator'
import {FormDetails} from '../form-control/form-control.component'
import {DeliveryForm} from './delivery-form'
import {CardForm} from './card-form'
import {firstToUpperCase, cardPattern, toNumber} from '../shared/strings'

@Component({
  selector: 'app-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.sass']
})
export class CheckoutPageComponent implements OnInit {
  orderLines: OrderLine[] = []
  totalPrice: number = 0
  loading: boolean = false
  deliveryForm: FormGroup
  cardForm: FormGroup
  deliveryFormDetails: FormDetails[] = DeliveryForm
  cardFormDetails: FormDetails[] = CardForm
  
  constructor(
    private zone:NgZone,
    private cartService: CartService,
    private orderService: OrderService,
    private fb: FormBuilder,
    private router: Router
  ) {}
  
  ngOnInit() {
    this.getOrderLines()
    this.buildDeliveryForm()
    this.buildCardForm()
    this.observeFormChanges()
  }
  
  getOrderLines() {
    this.cartService.getCart()
      .flatMap(cart => {
        if(cart.length < 1) {
          this.router.navigate([''])
        }
        return this.orderService.orderLines(cart)
      })
      .subscribe(order => {
        this.totalPrice = order.totalPrice
        this.orderLines = order.orderLines
      })
  }
  
  addItem(line: OrderLine) {
    this.cartService.addItem(line.productId)
  }
  
  subtractItem(line: OrderLine) {
    this.cartService.subtractItem(line.productId)
  }
  
  deleteItem(line: OrderLine) {
    this.cartService.deleteItem(line.productId)
  }
  
  buildDeliveryForm() {
    this.deliveryForm = this.fb.group({
      'name': ['', [Validators.required]],
      'street': ['', [Validators.required]],
      'zipCode': ['', [Validators.required]],
      'city': ['', [Validators.required]],
      'phone': ['', [Validators.required]],
      'email': ['', [EmailValidator.validEmail]]
    })
    // this.deliveryForm = this.fb.group({
    //   'name': ['Tore Skog Pettersen', [Validators.required]],
    //   'street': ['Aldor Ingebrigtsens veg 29', [Validators.required]],
    //   'zipCode': ['9010', [Validators.required]],
    //   'city': ['Tromsø', [Validators.required]],
    //   'phone': ['94177803', [Validators.required]],
    //   'email': ['toreskog@live.com', [EmailValidator.validEmail]]
    // })
  }
  
  buildCardForm() {
    this.cardForm = this.fb.group({
      'number': ['', [Validators.required]],
      'expMonth': ['', [Validators.required]],
      'expYear': ['', [Validators.required]],
      'cvc': ['', [Validators.required]],
    })
    // this.cardForm = this.fb.group({
    //   'number': ['', [Validators.required]],
    //   'expMonth': ['11', [Validators.required]],
    //   'expYear': ['22', [Validators.required]],
    //   'cvc': ['333', [Validators.required]],
    // })
  }
  
  observeFormChanges() {
    
    this.deliveryForm.get('name').valueChanges
      .distinctUntilChanged()
      .map(v => firstToUpperCase(v))
      .subscribe(v => this.deliveryForm.get('name').setValue(v))
  
    this.deliveryForm.get('city').valueChanges
      .distinctUntilChanged()
      .map(v => v.toUpperCase())
      .subscribe(v => this.deliveryForm.get('city').setValue(v))
    
    this.deliveryForm.get('zipCode').valueChanges
      .distinctUntilChanged()
      .map(v => toNumber(v))
      .map(v => v.substring(0, 4))
      .subscribe(v => this.deliveryForm.get('zipCode').setValue(v))
    
    this.cardForm.get('number').valueChanges
      .distinctUntilChanged()
      .map(v => toNumber(v))
      .map(v => cardPattern(v))
      .map(v => v.substring(0, 19))
      .subscribe(v => this.cardForm.get('number').setValue(v))
    
    this.cardForm.get('expMonth').valueChanges
      .distinctUntilChanged()
      .map(v => toNumber(v))
      .map(v => v.substring(0, 2))
      .subscribe(v => this.cardForm.get('expMonth').setValue(v))
    
    this.cardForm.get('expYear').valueChanges
      .distinctUntilChanged()
      .map(v => toNumber(v))
      .map(v => v.substring(0, 2))
      .subscribe(v => this.cardForm.get('expYear').setValue(v))
    
    this.cardForm.get('cvc').valueChanges
      .distinctUntilChanged()
      .map(v => toNumber(v))
      .map(v => v.substring(0, 3))
      .subscribe(v => this.cardForm.get('cvc').setValue(v))
  }
  
  touchAll() {
    Object.keys(this.deliveryForm.controls).forEach((key) => {
      this.deliveryForm.get(key).markAsTouched()
    })
    Object.keys(this.cardForm.controls).forEach((key) => {
      this.cardForm.get(key).markAsTouched()
    })
  }
  
  productTotal(line: OrderLine) {
    return line.unitPrice * line.quantity
  }
  
  createOrder(token: any) {
    if (!this.deliveryForm.valid) {
      return
    }
    
    let address: Address = {
      street: this.deliveryForm.get('street').value,
      zipCode: this.deliveryForm.get('zipCode').value,
      city: this.deliveryForm.get('city').value,
    }
    
    let customer: Customer = {
      name: this.deliveryForm.get('name').value,
      phone: this.deliveryForm.get('phone').value,
      email: this.deliveryForm.get('email').value,
      address: address
    }
    
    let order: Order = {
      stripeToken: token,
      customer: customer,
      orderLines: this.orderLines
    }
    
    this.orderService.createOrder(order)
      .subscribe(
        order => {
          this.cartService.deleteCart()
          this.loading = false
          this.router.navigate(['confirmation'])
        },
        err => this.handleError(err)
      )
  }
  
  createToken() {
    this.touchAll()
    if (!this.deliveryForm.valid) {
      return
    }
    this.loading = true;
    
    (<any>window).Stripe.card.createToken({
      number: this.cardForm.get('number').value,
      exp_month: this.cardForm.get('expMonth').value,
      exp_year: this.cardForm.get('expYear').value,
      cvc: this.cardForm.get('cvc').value
    }, (status: number, token: any) => {
      this.zone.run(() => {
        if(status < 200 || status >= 300) {
          this.handleError(token.error)
          return
        }
        
        this.createOrder(token.id)
      })
    })
  }
  
  handleError(err: any) {
    switch(err.code) {
      case 'incorrect_cvc':
        this.cardForm.get('cvc').setErrors({
          incorrectCvc: true
        })
        break
      case 'card_declined':
        this.cardForm.get('number').setErrors({
          cardDeclined: true
        })
        break
      case 'incorrect_number':
        this.cardForm.get('number').setErrors({
          incorrectNumber: true
        })
        break
      case 'expired_card':
        this.cardForm.get('number').setErrors({
          expiredCard: true
        })
        break
      case 'invalid_expiry_month':
        this.cardForm.get('expMonth').setErrors({
          invalidExpiryMonth: true
        })
        break
      case 'invalid_expiry_year':
        this.cardForm.get('expYear').setErrors({
          invalidExpiryYear: true
        })
        break
      case 'invalid_cvc':
        this.cardForm.get('cvc').setErrors({
          invalidCvc: true
        })
        break
      case 'processing_error':
      default:
    }
    this.loading = false
  }
}