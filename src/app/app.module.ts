import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'

import { AppComponent } from './app.component'
import {RouterModule} from '@angular/router'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'

import {NavbarModule} from './navbar'
import {ProductsPageModule} from './products-page';
import {StoreModule} from '@ngrx/store'
import {cartReducer} from './reducers/cart.reducer';
import {CheckoutPageModule} from './checkout-page'
import {ConfirmationPageModule} from './confirmation-page'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([]),
    NgbModule.forRoot(),
    StoreModule.provideStore({ cart: cartReducer }),
    NavbarModule,
    ProductsPageModule,
    CheckoutPageModule,
    ConfirmationPageModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
