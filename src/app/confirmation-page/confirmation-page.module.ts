import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationPageComponent } from './confirmation-page.component';
import {Route, RouterModule} from '@angular/router'

const ROUTES: Route[] = [
  { path: 'confirmation', component: ConfirmationPageComponent}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [ConfirmationPageComponent]
})
export class ConfirmationPageModule { }
