import { Action } from '@ngrx/store'
import {CartItem} from '../interfaces'

export const ADD_ITEM = 'Add item'
export const SUBTRACT_ITEM = 'Subtract item'
export const DELETE_ITEM = 'Remove item'
export const DELETE_CART = 'Delete cart'

export interface CartState {
  cart: CartItem[];
}

const storedState = JSON.parse(localStorage.getItem('cartState'))
const initialState: CartState = storedState ? storedState : {
  cart: []
}

export function cartReducer(state = initialState, action: Action) {
  let newState
  let cart = state.cart
  let itemIndex
  
  switch (action.type) {
    case ADD_ITEM:
      itemIndex = cart.findIndex(item => item.productId == action.payload)
      if(itemIndex >= 0){
        cart[itemIndex].quantity++
      } else {
        let newItem: CartItem = {
          productId: action.payload,
          quantity: 1
        }
        cart = [ ...cart, newItem]
      }
      
      newState = {
        cart: cart
      }
      break
    
    case SUBTRACT_ITEM:
      itemIndex = cart.findIndex(item => item.productId == action.payload)
      if(itemIndex >= 0){
        cart[itemIndex].quantity--
        if(cart[itemIndex].quantity <= 0){
          cart.splice(itemIndex, 1)
        }
      }
      
      newState = {
        cart: cart
      }
      break
    
    case DELETE_ITEM:
      itemIndex = cart.findIndex(item => item.productId == action.payload)
      if(itemIndex >= 0){
        cart.splice(itemIndex, 1)
      }
      
      newState = {
        cart: cart
      }
      break
    
    case DELETE_CART:
      newState = {
        cart: []
      }
      break
    
    default:
      newState = state
      break
  }
  
  localStorage.setItem('cartState', JSON.stringify(newState))
  return newState
}
