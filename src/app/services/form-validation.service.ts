import { Injectable } from '@angular/core';

@Injectable()
export class FormValidationService {

  constructor() { }
  
  public errorMessage(control) : string {
    for (let propertyName in control.errors) {
      if (control.errors.hasOwnProperty(propertyName) && control.touched) {
        return this.getValidatorErrorMessage(propertyName, control.errors[propertyName]);
      }
    }
    return null;
  }
  
  public getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'Du må fylle ut dette feltet.',
      'invalidCreditCard': 'Is invalid credit card number',
      'expiredCard': 'Kortet er utløpt',
      'cardDeclined': 'Ditt kort ble avvist',
      'incorrectNumber': 'Ditt kortnummer er feil',
      'incorrectCvc': 'Feil CVC',
      'invalidCvc': 'Ugyldig CVC',
      'invalidExpiryMonth': 'Ugyldig utløps måned',
      'invalidExpiryYear': 'Ugyldig utløps år',
      'invalidEmail': 'Ugyldig e-post adresse',
      'uniqueEmail': 'Email already registered',
      'invalidPassword': 'Invalid password. Password must be at least 60 characters long, and contain a toNumber.',
      'minlength': `Minimum length ${validatorValue.requiredLength}`,
      'maxlength': `Maximum length ${validatorValue.requiredLength}`
    };
    
    return config[validatorName];
  }

}
