export interface Product {
  id: string
  name: string
  price: number
  image: string
  category: string
  quantity: number
}

export interface CartItem {
  productId: string
  quantity: number
}

export interface Customer {
  name: string
  phone: number
  email: string
  address: Address
}

export interface Address {
  street: string
  zipCode: number
  city: string
}

export interface Order {
  id?: number
  stripeToken: string
  customer: Customer
  orderLines: OrderLine[]
}

export interface OrderLine {
  productId: string
  name: string
  unitPrice: number
  quantity: number
}