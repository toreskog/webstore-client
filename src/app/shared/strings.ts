
export const firstToUpperCase = (s: string) => s.replace(/\w\S*/g, txt => (txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()))
export const cardPattern = (s: string) => s.replace(/(.{4})/g, '$1 ').trim()
export const toNumber = (s: string) => s.replace(/[^\d]/g, '')


