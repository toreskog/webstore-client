import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../services/products.service'
import {Product} from '../interfaces'

@Component({
  selector: 'app-products',
  templateUrl: './products-page.component.html',
  styleUrls: ['products-page.component.sass']
})
export class ProductsPageComponent implements OnInit {
  products: Product[]
  
  constructor(
    private _productsService: ProductsService
  ) {}

  ngOnInit() {
    this._productsService.getAllProducts()
      .subscribe(p => this.products = p)
  }

}
