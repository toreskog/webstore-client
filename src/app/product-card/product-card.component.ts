import {Component, Input} from '@angular/core'
import {trigger, style, animate, transition, state} from '@angular/animations'
import {Product, CartItem} from '../interfaces'
import {Observable} from 'rxjs'
import {CartService} from '../services/cart.service'

@Component({
  selector: 'card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.sass'],
  animations: [
    trigger('addToCart', [
      state('active', style({'background-color': '#5cb85c', 'border-color': '#419641'})),
      state('inactive', style({'background-color': '#0275d8', 'border-color': '#0275d8'})),
      transition('inactive <=> active', animate(400))
    ])
  ]
})
export class ProductCardComponent {
  @Input() product: Product
  cart: Observable<CartItem>
  clicked: string = 'inactive'
  btnText: string = 'Legg i handelvogn'
  
  constructor(private cartService : CartService) {
  }

  addToCart(product: Product) {
    this.btnText = 'Lagt i handlevogn'
    this.clicked = 'active'
    Observable.timer(2000)
      .subscribe(() => {
        this.btnText = 'Legg i handlevogn'
        this.clicked = 'inactive'
      })
    this.cartService.addItem(product.id)
  }
}
