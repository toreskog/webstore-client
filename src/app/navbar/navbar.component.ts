import {Component, OnInit} from '@angular/core'
import {trigger, style, animate, transition, state} from '@angular/animations'
import {CartService} from '../services/cart.service'
import {Observable} from 'rxjs'

@Component({
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['./navbar.component.sass'],
  animations: [
    trigger('sizeChanged', [
      state('active', style({transform: 'scale(1.5)'})),
      state('inactive', style({transform: 'scale(1)'})),
      transition('inactive <=> active', animate(400))
    ])
  ]
})
export class NavbarComponent implements OnInit {
  isCollapsed: boolean = true
  cartSize: number = 0
  changed: string = 'inactive'

  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.cartService.cartSize()
      .subscribe(size => {
        this.cartSize = size
        this.changed = 'active'
        Observable.timer(1600)
          .subscribe(() => this.changed = 'inactive')
      })
  }
  
  public get menuIcon(): string {
    return this.isCollapsed ? '☰' : '✖';
  }
  
  
}
