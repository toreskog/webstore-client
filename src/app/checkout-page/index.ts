import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CheckoutPageComponent} from './checkout-page.component'
import {Route, RouterModule} from '@angular/router'
import {ReactiveFormsModule} from '@angular/forms'
import {FormControlModule} from '../form-control/index'
import {OrderService} from '../services/order.service'
import {SharedModule} from '../shared/shared.module'

const ROUTES: Route[] = [
  { path: 'checkout', component: CheckoutPageComponent}
]

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES),
    FormControlModule,
    SharedModule
  ],
  declarations: [
    CheckoutPageComponent
  ],
  providers: [
    OrderService
  ]
})
export class CheckoutPageModule { }