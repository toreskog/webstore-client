import { WebstoreClientPage } from './app.po';

describe('webstore-client App', () => {
  let page: WebstoreClientPage;

  beforeEach(() => {
    page = new WebstoreClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
