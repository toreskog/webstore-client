import {Component} from '@angular/core'


@Component({
  selector: 'spinner',
  template: `
    <div class="background">
        <div class="content">
          <i class="fa fa-spinner fa-spin fa-5x"></i>
          <h1>Venligst vent</h1>
        </div>
    </div>
  `,
  styles: [`
    .content {
        text-align: center;
        margin: 0;
        position: absolute;
        top: 45%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
    .background {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        z-index: 999;
        background-color: rgba(0,0,0,0.3);
    }
  `]
})
export class SpinnerComponent {}