import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControlComponent } from './form-control.component';
import {ReactiveFormsModule} from '@angular/forms'
import {FormValidationService} from '../services/form-validation.service'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    FormControlComponent
  ],
  exports: [
    FormControlComponent
  ],
  providers: [
    FormValidationService
  ]
})
export class FormControlModule { }