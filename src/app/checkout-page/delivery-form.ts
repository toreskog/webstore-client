import {FormDetails} from '../form-control/form-control.component'

export let DeliveryForm: FormDetails[] = [
  {
    key:'phone',
    label:'Mobiltelefon',
    placeholder:'Mobiltelefon',
    class: 'col-lg-6'
  }, {
    key:'email',
    label:'Epost',
    placeholder:'Epost',
    class: 'col-lg-6'
  }, {
    key:'name',
    label:'Navn',
    placeholder:'Navn',
    class: 'col-12'
  }, {
    key:'street',
    label:'Adresse',
    placeholder:'Adresse',
    class: 'col-12'
  }, {
    key:'zipCode',
    label:'Postnummer',
    placeholder:'Postnummer',
    class: 'col-4'
  }, {
    key:'city',
    label:'Poststed',
    placeholder:'Poststed',
    class: 'col-8'
  },
]