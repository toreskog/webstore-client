import {Injectable} from '@angular/core'
import {Product, CartItem} from '../interfaces'
import {BehaviorSubject} from 'rxjs'
import {Store} from '@ngrx/store'
import {CartState, ADD_ITEM, DELETE_ITEM, SUBTRACT_ITEM, DELETE_CART} from '../reducers/cart.reducer'
import {ProductsService} from './products.service'

@Injectable()
export class CartService {
  cartState: any = new BehaviorSubject<CartState>(null)
  
  constructor(
    private store: Store<CartState>
  ) {
    store.select('cart')
      .subscribe((state: CartState) => {
        this.cartState.next(state)
      })
  }
  
  getCart() {
    return this.cartState
      .map(state => state.cart)
  }
  
  addItem(id: string) {
    this.store.dispatch({type: ADD_ITEM, payload: id})
  }
  
  subtractItem(id: string) {
    this.store.dispatch({type: SUBTRACT_ITEM, payload: id})
  }
  
  deleteItem(id: string) {
    this.store.dispatch({type: DELETE_ITEM, payload: id})
  }
  
  deleteCart() {
    this.store.dispatch({type: DELETE_CART})
  }
  
  cartSize() {
    return this.cartState
      .map(state => {
        let size = 0
        state.cart.map(item => size += item.quantity)
        return size
      })
  }
  
}
