import {FormControl} from '@angular/forms'

export class EmailValidator {
  static validEmail(control: FormControl) {
    if(control.value === '') {
      return null
    }
    let regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    let valid = regEx.test(control.value)
    return valid ? null : { invalidEmail: true }
  }
}