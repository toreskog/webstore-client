import {Component, OnInit, Input, HostBinding} from '@angular/core';
import {FormGroup} from '@angular/forms'
import {FormValidationService} from '../services/form-validation.service'

export interface FormDetails {
  key: string,
  label: string,
  placeholder?: string,
  class?: string
}

@Component({
  selector: 'form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.sass'],
  
})
export class FormControlComponent implements OnInit {
  @Input() form: FormGroup
  @Input() details: FormDetails
  

  constructor(private formValidationService: FormValidationService) {
  }

  ngOnInit() {
  }
  
  errorMessage() {
    return this.formValidationService.errorMessage(this.form.controls[this.details.key])
  }

}
